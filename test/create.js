let assert 		= require('assert');
let chai 		= require('chai');
let chaiHttp 	= require('chai-http');
let io 			= require('socket.io-client');
let server 		= require('../serviceName');
let should 		= chai.should();


chai.use(chaiHttp);
const dataCreate = {
	firstname 	: 'stefanus',
	lastname 	: 'adhie',
	username 	: 'stefanusadhieputra',
	email 		: 'stefanusadhieputra@gmail.com',
	password 	: 'asdf1234*'
};

/**
 * Test the API
 */
/*describe('RESTAPI', () => {
	describe('#POST', () => {
		it('it should POST all the user', (done) => {
			chai.request(server)
				.post('/user')
				.send(dataCreate)
				.end((err, res) => {
					console.log(res.body)
					res.should.have.status(200);
					res.should.have.body('message');
					done();
				});
		});
	});
});*/


/**
 * Test the socket
 */
/*describe('SOCKET', () => {
	it('#create', (done) => {
		var options ={
			transports: ['websocket'],
			'force new connection': true
		};
		var client = io.connect('http://localhost:7002', options);

		// client.on('connect', () => {
			client.emit('create', dataCreate);

			client.on('create', (data) => {
				console.log(data);
				done();
			});
		// });
	});
});*/


var mongodbfunction = require('../app/schema');
describe('MONGODB FUNCTION', () => {
	it('#run function mongodb', (done) => {
		console.log(mongodbfunction)
		mongodbfunction.create();
	});
});
