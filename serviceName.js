const express 	= require('express');
const app 		= express();
/**
 * rest api
 */
const http 		= require('http');
const server 	= http.createServer(app);
/**
 * socket.io
 */
const io 		= require('socket.io')(server);
const config 	= require('./app/config');
const port 		= config.port;
const ip 		= config.ip;
const package 	= require('./package');
const service 	= package.name;
const version 	= package.version;


/**
 * mysql connection
 */
const mysql 	= require('./app/connection/mysql');


/**
 * mongodb connection
 */
const mongodb 	= require('./app/connection/mongodb')();


/**
 * restAPI
 */
var bodyParser 	= require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(function (req, res, next) {
	// Website you wish to allow to connect
	res.setHeader('Access-Control-Allow-Origin', '*');

	// Request methods you wish to allow
	res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

	// Request headers you wish to allow
	res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,token');

	// Set to true if you need the website to include cookies in the requests sent
	// to the API (e.g. in case you use sessions)
	res.setHeader('Access-Control-Allow-Credentials', true);

	// Pass to next layer of middleware
	next();
});
const restapi 	= require('./app/routes/restapi');
restapi(app)


/**
 * socket
 */
const socket 	= require('./app/routes/socket');
socket(io);


server.listen(port, ip,
	() => {
		console.log(`${service} version ${version} listening on ${ip}:${port}`);
	});


/**
 * export for testing
 */
module.exports = app;
