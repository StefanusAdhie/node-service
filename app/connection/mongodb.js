const mongoose 	= require('mongoose');
const database 	= require('../config').mongodb;


module.exports = () => {
	/**
	 * create connection to mongodb database
	 */
	mongoose.connect(database.uri, {useNewUrlParser: true}, err => {
		if(err) return console.log(`${err} mongodb error connection`);

		return console.log(`mongodb database success connect to ${database.uri}`);
	});
};
