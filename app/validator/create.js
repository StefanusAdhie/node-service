const Joi = require('joi');


const schema = Joi.object().keys({
	firstname	: Joi.string().required(),
	lastname 	: Joi.string().required(),
	username 	: Joi.string().required(),
	email 		: Joi.string().email().required(),
	password 	: Joi.string().required()
}).required();


module.exports = (data, callback) => {
	// You can also pass a callback which will be called synchronously with the validation result.
	Joi.validate(data, schema, (err, value) => {
		if(err) return callback(true, err.details);

		return callback(null, value);
	});  
};
