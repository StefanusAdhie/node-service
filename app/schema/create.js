const database 	= require('../connection/mongodb');
const mongoose 	= require('mongoose');
const schema 	= mongoose.Schema;


const createUserSchema = new schema({
	username: String,
	email: String,
	password: String,
	register: Date
});
const CreateUser = mongoose.model('users', createUserSchema, 'users');


const createUserDetailSchema = new schema({
	_id: schema.Types.ObjectId,
	firstname: String,
	lastname: String
});
const CreateUserDetail = mongoose.model('users_detail', createUserDetailSchema, 'users_detail');


module.exports = (data, callback) => {
	const createAuthor = new CreateUser({
		username: data.username,
		email: data.email,
		password: data.password,
		register: new Date()
	});
	const lastInsertID = createAuthor._id;

	createAuthor.save((err, res) => {
		if(err) return callback(true, err.errmsg);

		const createDetailAuthor = new CreateUserDetail({
			_id: new mongoose.mongo.ObjectId(lastInsertID),
			firstname: data.firstname,
			lastname: data.lastname
		});

		createDetailAuthor.save((_err, _res) => {
			if(_err) return callback(true, _err.errmsg);
			
			return callback(null, _res);
		});
	});
};
