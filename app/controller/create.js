const async 	= require('async');
const validator = require('../validator');
const procedure = require('../procedure');
const schema 	= require('../schema');


module.exports = (req, callback) => {
	async.waterfall([
		(next) => {
			validator.create(req, (err, value) => {
				if(err) return next(true, value[0].message);

				return next(null, value);
			});
		},
		/*(value, next) => {
			procedure.create(value, (err, res) => {
				if(err) return next(true, res);

				return next(null, res);
			});
		},*/
		(value, next) => {
			schema.create(value, (err, res) => {
				if(err) return next(true, res);

				return next(null, 'success');
			});
		}
	],
	(error, response) => {
		if(error) {
			let data = {
				headers: {
					statusCode 	: 400,
					message 	: response
				}
			};
			return callback(data);
		};

		let data = {
			headers: {
				statusCode 	: 200,
				message 	: response
			}
		};
		return callback(data);
	});
};
