module.exports = {
	/**
	 * service connection
	 */
	port 	: '7002',
	ip 	: '0.0.0.0',


	/**
	 * mysql database connection
	 */
	mysql 	: {
		host 		: 'localhost',
		user 		: 'root',
		password	: '',
		database 	: 'test_users',
	},


	/**
	 * mongodb database connection
	 */
	mongodb : {
		uri : 'mongodb://localhost:27017/test_users'
	}
};
