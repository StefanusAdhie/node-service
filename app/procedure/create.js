const database = require('../connection/mysql');


module.exports = (data, callback) => {
	database.query(
		'CALL user_create(?, ?, ?, ?, ?)',
		[data.firstname, data.lastname, data.username, data.email, data.password],
		(err, res) => {
			if(err) return callback(true, err.sqlMessage);

			return callback(null, res);
	});
};
