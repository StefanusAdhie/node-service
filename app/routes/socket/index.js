const controller= require('../../controller');


module.exports = (io) => {
	io.on('connection', socket => {
		socket.on('create', (data) => {
			controller.create(data, callback => {
				socket.emit('create', callback);
			});
		});
	});
};
